from fabric.api import env, task, run, sudo, put, cd

def grunt():
    sudo('npm install -g grunt-cli')
    sudo('npm install grunt')

def handlebars():
    sudo('sudo npm install handlebars -g')
    sudo('npm install grunt-contrib-handlebars --save-de')

def compass():
    sudo('apt-get install -q -y ruby')
    sudo('gem install --no-ri --no-rdoc sass -v 3.2.13')
    sudo('gem install --no-ri --no-rdoc compass -v 0.12.2')

@task
def all():
    sudo('npm install')
    grunt()
    handlebars()
    compass()
