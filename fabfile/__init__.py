from __future__ import unicode_literals
from fabric.api import env

from . import install

env.user = 'bruce'
env.code = '/home/%s/Machines/fullstackcoder' % env.user
env.branch = 'master'
